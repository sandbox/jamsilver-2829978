<?php
/**
 * @file
 * Implementation of a single SQL blob as an alternative field storage.
 */

/**
 * Determine if we should support revisions for a certain entity type.
 *
 * @param string $entity_type
 *   The entity type.
 * @return bool
 *   Indicates that revisions are enabled.
 */
function field_sql_blob_revisions_enabled($entity_type) {
  $enabled_types = variable_get('field_sql_blob_revisions_enabled', array());
  return !empty($enabled_types[$entity_type]);
}

/**
 * Implements hook_help().
 */
function field_sql_blob_storage_help($path, $arg) {
  switch ($path) {
    case 'admin/help#field_sql_blob_storage':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Field SQL Blob storage module stores field data in the database. A table is created for each entity type and all fields for an entity are stored in one big json-encoded blob. See the <a href="@field-help">Field module help page</a> for more information about fields.', array('@field-help' => url('admin/help/field'))) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_field_storage_info().
 */
function field_sql_blob_storage_field_storage_info() {
  return array(
    'field_sql_blob_storage' => array(
      'label' => t('SQL Blob storage'),
      'description' => t('Stores fields in the local SQL database, using per-entity-type tables.'),
    ),
  );
}

/**
 * Generate a table name for an entity type blob data table.
 *
 * @param string $entity_type
 *   The name of the entity type.
 *
 * @return string
 *   A string containing the generated name for the database table.
 */
function _field_sql_blob_storage_tablename($entity_type) {
  return "field_blob_data_{$entity_type}";
}

/**
 * Generate a table name for an entity_type revision archive table.
 *
 * @param string $entity_type
 *   The name of the entity type.
 *
 * @return string
 *   A string containing the generated name for the database table.
 */
function _field_sql_blob_storage_revision_tablename($entity_type) {
  return "field_blob_revision_{$entity_type}";
}

/**
 * Return the database schema for an entity_type.
 *
 * This may contain one or more tables.
 *
 * @param string $entity_type
 *   The entity type for which to generate a database schema.
 * @return array
 *   One or more tables representing the schema for the field.
 */
function _field_sql_blob_storage_schema($entity_type) {
  $current = array(
    'description' => "Data storage for sql blob fields for {$entity_type} entities.",
    'fields' => array(
      'entity_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The entity id this data is attached to',
      ),
      'revision_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'description' => 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
      ),
      'fields' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'All field_sql_blob field values json-encoded into one object.',
      ),
    ),
    'primary key' => array('entity_id'),
  );

  // Construct the revision table.
  $revision = $current;
  $revision['description'] = "Revision archive storage for sql blob fields for {$entity_type} entities.";
  $revision['primary key'] = array('entity_id', 'revision_id');
  $revision['fields']['revision_id']['not null'] = TRUE;
  $revision['fields']['revision_id']['description'] = 'The entity revision id this data is attached to';

  $schema = array(
    _field_sql_blob_storage_tablename($entity_type) => $current,
  );
  if (field_sql_blob_revisions_enabled($entity_type)) {
    $schema += array(
      _field_sql_blob_storage_revision_tablename($entity_type) => $revision,
    );
  }

  return $schema;
}

/**
 * Implements hook_field_storage_delete_field().
 */
function field_sql_blob_storage_field_storage_delete_field($field) {
  // Do nothing. Yes, this means that the old data remains on existing entities,
  // but it gets removed when they are next saved.
  // @TODO: Can we improve this?
}

/**
 * Implements hook_field_create_instance().
 *
 * This is not a field storage hook per se, but still works. If this instance
 * is the first on the entity type to use our storage, create the table.
 */
function field_sql_blob_storage_field_create_instance($instance) {
  $field = field_info_field($instance['field_name']);
  if ($field['storage']['type'] === 'field_sql_blob_storage') {
    $entity_type = $instance['entity_type'];
    $schema = _field_sql_blob_storage_schema($entity_type);
    $table_was_created = FALSE;
    foreach ($schema as $name => $table) {
      if (!db_table_exists($name)) {
        $table_was_created = TRUE;
        db_create_table($name, $table);
      }
    }
    if ($table_was_created) {
      drupal_get_schema(NULL, TRUE);
    }
  }
}

/**
 * Implements hook_field_storage_load().
 */
function field_sql_blob_storage_field_storage_load($entity_type, $entities, $age, $fields, $options) {
  $load_current = $age == FIELD_LOAD_CURRENT;

  if (!$load_current && !field_sql_blob_revisions_enabled($entity_type)) {
    throw new Exception("Cannot load revisions for {$entity_type} entities. Revision tables are disabled for this entity type.");
  }

  $ids = array();
  foreach ($fields as $field_id => $ids_per_field) {
    $ids += drupal_map_assoc($ids_per_field);
  }
  $ids = array_values($ids);

  if ($load_current) {
    $id_key = 'entity_id';
    $table = _field_sql_blob_storage_tablename($entity_type);
  }
  else {
    $id_key = 'revision_id';
    $table = _field_sql_blob_storage_revision_tablename($entity_type);
  }

  $results = db_select($table, 't')
    ->fields('t')
    ->condition($id_key, $ids, 'IN')
    ->execute()
    ->fetchAllAssoc($id_key, PDO::FETCH_ASSOC);

  foreach (array_keys($results) as $key) {
    $results[$key]['fields'] = json_decode($results[$key]['fields'], TRUE);
    if (!is_array($results[$key]['fields'])) {
      $results[$key]['fields'] = array();
    }
  }

  foreach ($fields as $field_id => $ids) {
    $field = field_info_field_by_id($field_id);
    $field_name = $field['field_name'];
    $langcodes = field_available_languages($entity_type, $field);

    foreach ($ids as $id) {
      if (!empty($results[$id])) {
        $row = &$results[$id]['fields'];
        if (!empty($row[$field_name])) {
          foreach ($langcodes as $langcode) {
            if (isset($row[$field_name][$langcode])) {
              $items = &$row[$field_name][$langcode];
              if (($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) && count($items) > $field['cardinality']) {
                array_splice($items, 0, $field['cardinality']);
              }
              $entities[$id]->{$field_name}[$langcode] = array_values($items);
              unset($items);
            }
          }
        }
        unset($row);
      }
    }
  }
}

/**
 * Implements hook_field_storage_write().
 */
function field_sql_blob_storage_field_storage_write($entity_type, $entity, $op, $fields) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  if (!isset($vid)) {
    $vid = $id;
  }

  $table = _field_sql_blob_storage_tablename($entity_type);
  $revision_table = _field_sql_blob_storage_revision_tablename($entity_type);

  // If updating, fetch the existing and delete ready for a fresh insert.
  $existing_data = array();
  if ($op == FIELD_STORAGE_UPDATE) {
    $row = db_select($table, 't')
      ->fields('t')
      ->condition('entity_id', $id)
      ->execute()
      ->fetchObject();
    if ($row && !empty($row->fields)) {
      $existing_data = json_decode($row->fields, TRUE);
      if (!is_array($existing_data)) {
        $existing_data = array();
      }
    }
    db_delete($table)
      ->condition('entity_id', $id)
      ->execute();
    if (field_sql_blob_revisions_enabled($entity_type)) {
      db_delete($revision_table)
        ->condition('entity_id', $id)
        ->condition('revision_id', $vid)
        ->execute();
    }
  }

  // Merge incoming values into existing values following standard core logic.
  $new_data = array();
  foreach ($fields as $field_id) {
    $field = field_info_field_by_id($field_id);
    $field_name = $field['field_name'];
    if (!empty($entity->{$field_name})) {
      foreach (field_available_languages($entity_type, $field) as $langcode) {
        if (isset($entity->{$field_name}[$langcode])) {
          $new_data[$field_name][$langcode] = $entity->{$field_name}[$langcode];
        }
        elseif (isset($existing_data[$field_name][$langcode])) {
          $new_data[$field_name][$langcode] = $existing_data[$field_name][$langcode];
        }
      }
    }
  }

  $record = array(
    'entity_id' => $id,
    'revision_id' => $vid,
    'fields' => json_encode($new_data),
  );
  db_insert($table)->fields($record)->execute();
  if (field_sql_blob_revisions_enabled($entity_type)) {
    db_insert($revision_table)->fields($record)->execute();
  }
}

/**
 * Implements hook_field_storage_delete().
 *
 * This function deletes data for all fields for an entity from the database.
 */
function field_sql_blob_storage_field_storage_delete($entity_type, $entity, $fields) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  if (!isset($vid)) {
    $vid = $id;
  }
  $table = _field_sql_blob_storage_tablename($entity_type);
  db_delete($table)
    ->condition('entity_id', $id)
    ->execute();
  if (field_sql_blob_revisions_enabled($entity_type)) {
    $revision_table = _field_sql_blob_storage_revision_tablename($entity_type);
    db_delete($revision_table)
      ->condition('entity_id', $id)
      ->condition('revision_id', $vid)
      ->execute();
  }
}

/**
 * Implements hook_field_storage_purge().
 *
 * This function deletes data from the database for a single field on
 * an entity.
 */
function field_sql_blob_storage_field_storage_purge($entity_type, $entity, $field, $instance) {
  // Do nothing. Yes, this means that the old data remains on existing entities,
  // but it gets removed when they are next saved.
  // @TODO: Can we improve this?
}

/**
 * Implements hook_field_storage_query().
 */
function field_sql_blob_storage_field_storage_query(EntityFieldQuery $query) {
  // To support field_has_data() and field_purge_batch() we do not throw an
  // error for a mere existence check. Anything more complex fails noisily.
  if (empty($query->fieldConditions) && empty($query->propertyConditions) && empty($query->fieldMetaConditions)) {
    return array();
  }
  else {
    // If you want to use EFQ then you need to use a different field storage
    // module. We store our data as an encoded blob, so cannot support this.
    throw new EntityFieldQueryException('field_sql_blob fields do not support EntityFieldQuery.');
  }
}

/**
 * Implements hook_field_storage_delete_revision().
 *
 * This function actually deletes the data from the database.
 */
function field_sql_blob_storage_field_storage_delete_revision($entity_type, $entity, $fields) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  if (!isset($vid)) {
    $vid = $id;
  }
  if (field_sql_blob_revisions_enabled($entity_type)) {
    $revision_table = _field_sql_blob_storage_revision_tablename($entity_type);
    db_delete($revision_table)
      ->condition('entity_id', $id)
      ->condition('revision_id', $vid)
      ->execute();
  }
}

/**
 * Implements hook_field_storage_delete_instance().
 *
 * This function deletes the blob table if this was the last instance using it.
 */
function field_sql_blob_storage_field_storage_delete_instance($instance) {
  $field = field_info_field($instance['field_name']);
  if ($field['storage']['type'] === 'field_sql_blob_storage') {
    $fields = field_read_fields(array(), array('include_deleted' => FALSE, 'include_inactive' => TRUE));
    $instances = field_read_instances(array(), array('include_deleted' => FALSE, 'include_inactive' => TRUE));
    $supported_entity_types = array();
    foreach ($instances as $instance) {
      if (isset($fields[$instance['field_name']])) {
        $field = $fields[$instance['field_name']];
        if ($field['storage']['type'] === 'field_sql_blob_storage') {
          $supported_entity_types[$instance['entity_type']] = $instance['entity_type'];
        }
      }
    }
    if (!isset($supported_entity_types[$instance['entity_type']])) {
      $entity_type = $instance['entity_type'];
      $table = _field_sql_blob_storage_tablename($entity_type);
      db_drop_table($table);
      $revision_table = _field_sql_blob_storage_revision_tablename($entity_type);
      if (db_table_exists($revision_table)) {
        db_drop_table($revision_table);
      }
    }
  }
}
