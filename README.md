Field SQL Blob Storage
======================

This module provides an alternative field storage backend. A table is
created on a per-entity-type basis, and all SQL Blob fields for each
entity are stored in one big serialized field.

This substantially reduces the number of database tables and database
queries. It is an alternative to the caching approach offered by 
entitycache.


Trade-offs
----------
Filtering and sorting (whether by EntityFieldQuery or by views) is not
supported for obvious reasons.
